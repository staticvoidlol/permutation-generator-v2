# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Allows user to specify field names and their allowed values, and the tool will generate a CSV file with all the possible permutations.
Only for Windows.

### How to use the tool ###

1. Download the latest version in the /Releases folder.
2. Run the executable
3. Enter field names in the left-hand column, and allowed field values as a comma separated list in the right-hand column.

### Example ###

Suppose we have a game that consists of 3 rounds, and in each round there is a winner.

Suppose the game has the following participants in each of the rounds:

** Round 1 **

* John
* Steven
* Peter

** Round 2**

* John
* Mary

** Round 3**

* Mary
* Steven
* Peter
* John
* Gavin

To list out all the possible permuations of how the full game could play out, the tool would be set up as follows:

![Capture.PNG](https://bitbucket.org/repo/AKnbgx/images/3192346558-Capture.PNG)

Click the "Generate CSV" button and the tool would output the following TAB delimited file:

[Example Output File](https://bitbucket.org/staticvoidlol/permutation-generator-v2/src/9a3ae3d726747589cf43ce8514bce200f707a9ea/ExampleOutput/201608311148260245.csv?at=master&fileviewer=file-view-default)

Please note that any delimiter can be specified in the "Output File Delimiter" textbox. ("\t" is a special case that will be processed as the TAB character)

Clicking the "Open Folder" button will open up in explorer the folder in which the CSV file was generated, and the file will be selected.