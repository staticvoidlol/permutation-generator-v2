﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PermutationGeneratorV2
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        // Gets the directory in which the executable resides
        private DirectoryInfo GetAppDirectory()
        {
            return (new FileInfo(Application.ExecutablePath)).Directory;
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            // Init logger
            Logger.SetListBoxLog(listBoxLog);
            Logger.SetLogFileDir(GetAppDirectory());
            Logger.Log(@"Initialised.");

            // Populate export
            textBoxExportFileName.Text = Path.Combine(GetAppDirectory().FullName, Helper.GetTimeFormattedFileName(DateTime.Now) + ".csv");            
        }

        // Ensure that we catch the form closing event and dispose properly of the logger
        private void Form1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Logger.DisposeLogger();
        }

        private void buttonGenerateCsv_Click(object sender, EventArgs e)
        {
            Logger.Log("Verifying parameters...");
            // Fields to pass to permutator
            List<Field> fields = new List<Field>();

            for (int i = 0; i < dataGridViewFields.Rows.Count; i++)
            {
                if (dataGridViewFields.Rows[i].Cells[0].Value != null)
                {
                    string fieldName = dataGridViewFields.Rows[i].Cells[0].Value.ToString();

                    string fieldValuesStr = dataGridViewFields.Rows[i].Cells[1].Value.ToString();

                    List<string> fieldValues = fieldValuesStr.Split(',').ToList();
                    fields.Add(new Field(fieldName, fieldValues));                  
                }
            }

            Logger.Log("Generating permutations...");
            Permutator p = new Permutator(fields);

            Logger.Log("Writing to file...");

            // Handle case where TAB is delimiter (\t)
            string delimiterToUse = this.textBoxDelimiter.Text == @"\t" ? "    " : this.textBoxDelimiter.Text;

            List<string> perms = p.GetPermutationsList(delimiterToUse);
            using(StreamWriter w = new StreamWriter(this.textBoxExportFileName.Text))
            {
                foreach (string s in perms)
                    w.WriteLine(s);
            }

            Logger.Log("Done.");
        }

        // Opens a directory in explorer and selects the relevant file - 
        private void buttonOpenContainingFolder_Click(object sender, EventArgs e)
        {
            // suppose that we have a test.txt at E:\
            string filePath = textBoxExportFileName.Text;

            if (!File.Exists(filePath))
            {
                return;
            }

            // Combine the arguments together
            // it doesn't matter if there is a space after ','
            string argument = "/select, \"" + filePath + "\"";

            System.Diagnostics.Process.Start("explorer.exe", argument);
        }
    }
}
