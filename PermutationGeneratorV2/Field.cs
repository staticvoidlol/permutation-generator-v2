﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace PermutationGeneratorV2
{
    // Contains the data about a field and its allowed values
    public class Field
    {
        public string Name;
        public List<string> Values;

        public Field(string name, List<string> values)
        {
            Name = name;
            Values = values;
        }
    }

    // A simple node class that allows a parent child structure to be created
    public class Node
    {
        public Node Parent;
        public List<Node> Children = new List<Node>();
        public Field Field;
        public int ValueIdx;
        public string Value;

        public Node(Node parent, Field field, int valueIdx, string value)
        {
            Parent = parent;
            Field = field;
            ValueIdx = valueIdx;
            Value = value;
        }

        public void AddChild(Node child)
        {
            Children.Add(child);
        }
    }

    // Generates a multinomial tree of nodes that represent all the possible permutations of a list of fields and their values
    public class Permutator
    {
        public const string RootNodeName = "ROOT";
        public List<Field> Fields;
        public List<Node> LeafNodes;

        public Permutator(List<Field> fields)
        {
            Fields = fields;
            Go();
        }

        protected void Go()
        {
            // Rootnode
            Node root = new Node(null, null, 0, RootNodeName);
            List<Node> rootList = new List<Node>() { root };
            LeafNodes = Recurse(rootList, 0);
            
            List<string> a = GetPermutationsList(",");
            Logger.Log("Done");
        }

        // Recursive function that generates the tree
        protected List<Node> Recurse(List<Node> parentNodes, int currFieldIdx)
        {
            Field currField = this.Fields[currFieldIdx];
            List<Node> newChildren = new List<Node>();

            for (int j = 0; j < parentNodes.Count; j++)
            {
                Node currParentNode = parentNodes[j];

                for (int i = 0; i < currField.Values.Count; i++)
                {
                    Node child = new Node(currParentNode, currField, i, currField.Values[i]);
                    newChildren.Add(child);
                    currParentNode.AddChild(child);
                }
            }

            if (currFieldIdx < this.Fields.Count - 1)
            {
                newChildren = Recurse(newChildren, currFieldIdx + 1);                
            }

            return newChildren;
        }

        // Allows an outside caller to get the permutations as a list, where each list entry represents a permutation
        public List<string> GetPermutationsList(string delimiter, bool includeHeader = true)
        {
            List<string> rows = new List<string>();

            if (includeHeader)
            {
                // Join all field names into single string based on name column
                string headerLine = string.Join(delimiter, this.Fields.Select(f => f.Name));
                rows.Add(headerLine);
            }

            foreach(Node n in LeafNodes)
            {
                List<string> currRowVals = new List<string>();
                bool hasReachedRoot = false;
                Node currNode = n;

                while(!hasReachedRoot)
                {
                    currRowVals.Add(currNode.Value);
                    currNode = currNode.Parent;
                    if (currNode.Value == RootNodeName)
                    {
                        hasReachedRoot = true;
                        currRowVals.Reverse();
                        string vals = string.Join(delimiter, currRowVals);
                        rows.Add(vals);
                    }
                }
            }

            return rows;
        }
       
    }
}
