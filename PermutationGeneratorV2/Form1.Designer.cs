﻿namespace PermutationGeneratorV2
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxLog = new System.Windows.Forms.GroupBox();
            this.listBoxLog = new System.Windows.Forms.ListBox();
            this.groupBoxSettings = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridViewFields = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxExportFileName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxDelimiter = new System.Windows.Forms.TextBox();
            this.buttonGenerateCsv = new System.Windows.Forms.Button();
            this.buttonOpenContainingFolder = new System.Windows.Forms.Button();
            this.Field = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AllowedValues = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBoxLog.SuspendLayout();
            this.groupBoxSettings.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFields)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxLog
            // 
            this.groupBoxLog.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxLog.Controls.Add(this.listBoxLog);
            this.groupBoxLog.Location = new System.Drawing.Point(12, 284);
            this.groupBoxLog.Name = "groupBoxLog";
            this.groupBoxLog.Size = new System.Drawing.Size(808, 146);
            this.groupBoxLog.TabIndex = 0;
            this.groupBoxLog.TabStop = false;
            this.groupBoxLog.Text = "Log";
            // 
            // listBoxLog
            // 
            this.listBoxLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxLog.FormattingEnabled = true;
            this.listBoxLog.Location = new System.Drawing.Point(6, 19);
            this.listBoxLog.Name = "listBoxLog";
            this.listBoxLog.Size = new System.Drawing.Size(796, 121);
            this.listBoxLog.TabIndex = 0;
            // 
            // groupBoxSettings
            // 
            this.groupBoxSettings.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxSettings.Controls.Add(this.buttonOpenContainingFolder);
            this.groupBoxSettings.Controls.Add(this.buttonGenerateCsv);
            this.groupBoxSettings.Controls.Add(this.textBoxDelimiter);
            this.groupBoxSettings.Controls.Add(this.label4);
            this.groupBoxSettings.Controls.Add(this.textBoxExportFileName);
            this.groupBoxSettings.Controls.Add(this.label3);
            this.groupBoxSettings.Location = new System.Drawing.Point(13, 4);
            this.groupBoxSettings.Name = "groupBoxSettings";
            this.groupBoxSettings.Size = new System.Drawing.Size(807, 72);
            this.groupBoxSettings.TabIndex = 1;
            this.groupBoxSettings.TabStop = false;
            this.groupBoxSettings.Text = "Settings";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.dataGridViewFields);
            this.groupBox1.Location = new System.Drawing.Point(13, 82);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(807, 196);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Fields and Values";
            // 
            // dataGridViewFields
            // 
            this.dataGridViewFields.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewFields.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewFields.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewFields.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Field,
            this.AllowedValues});
            this.dataGridViewFields.Location = new System.Drawing.Point(6, 19);
            this.dataGridViewFields.Name = "dataGridViewFields";
            this.dataGridViewFields.Size = new System.Drawing.Size(795, 171);
            this.dataGridViewFields.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Export File Name";
            // 
            // textBoxExportFileName
            // 
            this.textBoxExportFileName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxExportFileName.Location = new System.Drawing.Point(113, 17);
            this.textBoxExportFileName.Name = "textBoxExportFileName";
            this.textBoxExportFileName.Size = new System.Drawing.Size(475, 20);
            this.textBoxExportFileName.TabIndex = 1;
            this.textBoxExportFileName.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Output File Delimiter";
            // 
            // textBoxDelimiter
            // 
            this.textBoxDelimiter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxDelimiter.Location = new System.Drawing.Point(113, 43);
            this.textBoxDelimiter.Name = "textBoxDelimiter";
            this.textBoxDelimiter.Size = new System.Drawing.Size(475, 20);
            this.textBoxDelimiter.TabIndex = 3;
            this.textBoxDelimiter.Text = "\\t";
            this.textBoxDelimiter.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // buttonGenerateCsv
            // 
            this.buttonGenerateCsv.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonGenerateCsv.Location = new System.Drawing.Point(594, 17);
            this.buttonGenerateCsv.Name = "buttonGenerateCsv";
            this.buttonGenerateCsv.Size = new System.Drawing.Size(103, 46);
            this.buttonGenerateCsv.TabIndex = 4;
            this.buttonGenerateCsv.Text = "Generate CSV";
            this.buttonGenerateCsv.UseVisualStyleBackColor = true;
            this.buttonGenerateCsv.Click += new System.EventHandler(this.buttonGenerateCsv_Click);
            // 
            // buttonOpenContainingFolder
            // 
            this.buttonOpenContainingFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOpenContainingFolder.Location = new System.Drawing.Point(698, 17);
            this.buttonOpenContainingFolder.Name = "buttonOpenContainingFolder";
            this.buttonOpenContainingFolder.Size = new System.Drawing.Size(103, 46);
            this.buttonOpenContainingFolder.TabIndex = 5;
            this.buttonOpenContainingFolder.Text = "Open Folder";
            this.buttonOpenContainingFolder.UseVisualStyleBackColor = true;
            this.buttonOpenContainingFolder.Click += new System.EventHandler(this.buttonOpenContainingFolder_Click);
            // 
            // Field
            // 
            this.Field.HeaderText = "Field Name";
            this.Field.Name = "Field";
            // 
            // AllowedValues
            // 
            this.AllowedValues.HeaderText = "Allowed Values";
            this.AllowedValues.Name = "AllowedValues";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(832, 442);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBoxSettings);
            this.Controls.Add(this.groupBoxLog);
            this.MinimumSize = new System.Drawing.Size(848, 480);
            this.Name = "FormMain";
            this.Text = "Permutation Generator V2";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.groupBoxLog.ResumeLayout(false);
            this.groupBoxSettings.ResumeLayout(false);
            this.groupBoxSettings.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFields)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxLog;
        private System.Windows.Forms.ListBox listBoxLog;
        private System.Windows.Forms.GroupBox groupBoxSettings;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridViewFields;
        private System.Windows.Forms.TextBox textBoxDelimiter;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxExportFileName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonGenerateCsv;
        private System.Windows.Forms.Button buttonOpenContainingFolder;
        private System.Windows.Forms.DataGridViewTextBoxColumn Field;
        private System.Windows.Forms.DataGridViewTextBoxColumn AllowedValues;
    }
}

