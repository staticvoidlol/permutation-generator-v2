﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace PermutationGeneratorV2
{
    public static class Logger
    {
        //The listbox to which we post our log entries
        public static ListBox LogBox;
        public static FileInfo Logfile;
        public static StreamWriter LogWriter;

        //Should be called from the main form to subscribe our listbox 
        public static void SetListBoxLog(ListBox logBox)
        {
            LogBox = logBox;
        }

        public static void SetLogFileDir(DirectoryInfo logFileDirectory)
        {
            if (!logFileDirectory.Exists)
                Directory.CreateDirectory(logFileDirectory.FullName);

            Logfile = new FileInfo(Path.Combine(logFileDirectory.FullName, Helper.GetTimeFormattedFileName(DateTime.Now) + ".txt"));
            LogWriter = new StreamWriter(Logfile.FullName);
        }

        public static void Log(string message)
        {
            try
            {
                string logMessage = Helper.GetTimeFormatted(DateTime.Now) + @" - ";

                logMessage += message;

                if (LogBox != null)
                {
                    //Thanks Antonio. You are 733tz0|^
                    LogBox.Items.Insert(0, logMessage);

                    //Refresh so we update before continuing stack
                    LogBox.Refresh();
                }

                LogWriter.WriteLine(logMessage);
            }
            catch
            {
                MessageBox.Show("How did we get here? Logger exception.");
            }
        }

        public static void DisposeLogger()
        {
            LogWriter.Flush();
            LogWriter.Close();
            LogWriter.Dispose();
        }
                
    }
}
