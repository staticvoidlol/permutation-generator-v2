﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace PermutationGeneratorV2
{
    public static class Helper
    {
        //Gets a properly formatted string of the given datetime
        public static string GetTimeFormatted(DateTime dt)
        {
            string f = "";

            f += dt.Year.ToString() +@"/";
            f += GetIndexedNumber(dt.Month, 12) + @"/";
            f += GetIndexedNumber(dt.Day, 31) + @" ";
            f += GetIndexedNumber(dt.Hour, 24) + @":";
            f += GetIndexedNumber(dt.Minute, 60) + @":";
            f += GetIndexedNumber(dt.Second, 60) + @":";
            f += GetIndexedNumber(dt.Millisecond, 1000);

            return f;
        }

        public static string GetTimeFormattedYYYYMMDD(DateTime dt)
        {
            string f = "";

            f += dt.Year.ToString();
            f += GetIndexedNumber(dt.Month, 12);
            f += GetIndexedNumber(dt.Day, 31);

            return f;
        }

        public static string GetTimeFormattedFileName(DateTime dt)
        {
            string f = "";

            f += dt.Year.ToString();
            f += GetIndexedNumber(dt.Month, 12);
            f += GetIndexedNumber(dt.Day, 31);
            f += GetIndexedNumber(dt.Hour, 24);
            f += GetIndexedNumber(dt.Minute, 60);
            f += GetIndexedNumber(dt.Second, 60);
            f += GetIndexedNumber(dt.Millisecond, 1000);            

            return f;
        }

        //Gets a string padded with 0's on the left up until the max 
        public static string GetIndexedNumber(int currIdx, int maxIdx)
        {
            string f = "";

            char[] m = maxIdx.ToString().ToCharArray();
            char[] c = currIdx.ToString().ToCharArray();

            for (int i = 0; i < m.Length - c.Length ; i++)
            {
                f += "0";
            }

            f += currIdx.ToString();

            return f;
        }

        public static string GetUnindexedTimeStringFileName(string indexedString)
        {
            return indexedString.Substring(18, indexedString.Length - 18);
        }

        public static void CopyDirectory(DirectoryInfo source, DirectoryInfo targetBase)
        {
            // Create directory structure
            foreach(string srcFolder in Directory.EnumerateDirectories(source.FullName, "*", SearchOption.AllDirectories))
            {
                Directory.CreateDirectory(srcFolder.Replace(source.Parent.FullName, targetBase.FullName));
            }

            foreach(string srcFile in Directory.EnumerateFiles(source.FullName, "*", SearchOption.AllDirectories))
            {
                File.Copy(srcFile, srcFile.Replace(source.Parent.FullName, targetBase.FullName));
            }
        }
    }
}
